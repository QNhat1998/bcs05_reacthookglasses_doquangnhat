import React, { memo } from 'react'

 function GlassesList({setGlasses,data}) {
    console.log('render glasseslist');
    return (
        <div className='row g-3 bg-light p-4'>
            {
                data.map((glasses, index) => {
                    return <div key={index} className="col-4">
                        <img style={{ cursor: 'pointer' }} className='img-fluid' src={glasses.url} onClick={()=>setGlasses(glasses)} alt="" />
                    </div>
                })
            }
        </div>
    )
}

export default memo(GlassesList)
