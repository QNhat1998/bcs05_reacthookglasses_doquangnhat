import { useState } from 'react';
import './App.css';
import dataGlasses from './dataGlasses.json';
import GlassesList from './GlassesList';
import Model from './Model';

const init = {
  "id": '',
  "price": '',
  "name": "",
  "url": "",
  "desc": ""
}

function App() {
  const [glasses, setGlasses] = useState(init);
  console.log(glasses);
  console.log('render app');
  return (
    <div className="App">
      <div className="container-fluid h-100">
        <div className="row h-100 align-items-center px-5">
          <div className="col-6">
            <Model glasses={glasses}/>
          </div>
          <div className="col-6">
            <button className='btn btn-danger mb-5 w-100' onClick={()=>setGlasses(init)}>Reset</button>
            <GlassesList data={dataGlasses} setGlasses={setGlasses} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
