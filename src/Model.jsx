import React from 'react'

export default function Model({ glasses }) {
    const { url, desc, name, price } = glasses;
    console.log('render model');
    return (
        <div className="card mx-auto" style={{ width: '30rem' }}>
            <img src="./glassesImage/model.jpg" className="card-img-top" alt="..." />
            {url.length > 0 && <img src={url} className='position-absolute opacity-75 mx-auto' style={{
                left: 0,
                right: 0,
                top: 140,
                width: '300px'
            }} alt='glasses' />}
            <div className="card-body text-start">
                <h5 className="card-title">Tên: {name}</h5>
                <p className="card-text">Mô tả sản phẩm: {desc}</p>
                <p className="card-text">Giá: {price}$</p>
            </div>
        </div>
    )
}
